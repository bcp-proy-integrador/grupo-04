import { Component, OnInit } from '@angular/core';

import { PersonaService } from '../../servicios/persona.service';
import { Persona } from '../../models/persona';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-grabar',
  templateUrl: './grabar.component.html',
  styleUrls: ['./grabar.component.css']
})
export class GrabarComponent implements OnInit {


  persona: Persona = {
    id: 0,
    nombre: '',
    apellido: '',
    direccion: '',
    telefono: '',
    email: '',
    pais: {
      id:0,
      nombre:''
    }
  };


  listPaises = [
    { id: 1, nombre: "Peru" },
    { id: 2, nombre: "Argentina" },
    { id: 3, nombre: "Brasil" },
  ];

  edit: boolean = false;

  constructor(private personaService: PersonaService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {

    const params = this.activatedRoute.snapshot.params;
    if (params.id) {
      this.personaService.getPersona(params.id)
        .subscribe(
          res => {
            console.log(res);
            this.persona = res;
            this.edit = true;
          },
          err => console.log(err)
        )
    }

  }


  saveNewPersona() {
    delete this.persona.id;
    
    this.personaService.savePersona(this.persona)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/personas']);
        },
        err => console.error(err)
      )
  }



  updatePersona() {

    this.personaService.updatePersona(this.persona)
      .subscribe(
        res => { 
          console.log(res);
          this.router.navigate(['/personas']);
        },
        err => console.error(err)
      )
  }






}
