import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Persona } from '../models/persona';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  API_URI = '/wspersonas';

  constructor(private http: HttpClient) { }

  getPersonas() {
    return this.http.get(`${this.API_URI}`);
  }

  getPersona(id: number) {
    return this.http.get(`${this.API_URI}/${id}`);
  }

  deletePersona(id: number) {
    return this.http.delete(`${this.API_URI}/${id}`);
  }

  savePersona(persona: Persona) {
    return this.http.post(`${this.API_URI}`, persona);
  }

  updatePersona(persona: Persona): Observable<Persona> {   
    return this.http.put(`${this.API_URI}`, persona); 
  }

}
