import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListarComponent } from './componentes/listar/listar.component';
import { NavegarComponent } from './componentes/navegar/navegar.component';
import { GrabarComponent } from './componentes/grabar/grabar.component';


import { PersonaService } from './servicios/persona.service';

@NgModule({
  declarations: [
    AppComponent,
    ListarComponent,
    NavegarComponent,
    GrabarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [PersonaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
