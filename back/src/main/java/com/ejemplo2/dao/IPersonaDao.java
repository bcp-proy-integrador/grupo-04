package com.ejemplo2.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ejemplo2.model.Persona;

public interface IPersonaDao extends JpaRepository<Persona, Integer> {

}
