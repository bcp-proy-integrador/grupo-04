package com.ejemplo2.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ejemplo2.model.Pais;


public interface IPaisDao extends JpaRepository<Pais, Integer> {

}
