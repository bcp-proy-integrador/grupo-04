package com.ejemplo2.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ejemplo2.model.Pais;
import com.ejemplo2.model.Usuario;


public interface IUsuarioDao extends JpaRepository<Usuario, Integer> {

	public Optional<Usuario> findByUsername(String username);
	
}
