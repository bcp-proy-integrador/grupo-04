package com.ejemplo2.commons;

import java.io.Serializable;
import java.util.List;

public interface IGenericService<T, ID extends Serializable> {
	
	T save(T entity);//sirve para registrar y actualizar, aca le pasamos una "entidad"
	
	void delete(ID id);//sirve pa eliminar, aca le pasamos el "id" 
	
	T get(ID id);//cuando queramos consultar y obtener valor, aca le pasamos el "id" 

	List<T> getAll();//Devuelve todo los registros de "Persona"
}
