package com.ejemplo2.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ejemplo2.model.Persona;
import com.ejemplo2.service.IPaisService;
import com.ejemplo2.service.IPersonaService;


@Controller
public class PersonaController {
	
	
	@Autowired
	private IPersonaService personaService;
	
	
	@Autowired
	private IPaisService paisService;
	
	
	
	@GetMapping({"/","/login"})
	public String index() {
		return "index";
	}
	

	@RequestMapping("/listpersona")
	public String index(Model model) {
		
		model.addAttribute("listado", personaService.getAll());
		
		return "inicio";
	}
	/*--------------*/
	
	
	
	@GetMapping("/save/{id}")
	public String showSave(@PathVariable("id") Integer id, Model model) {
		
		model.addAttribute("listPaises", paisService.getAll());
		
		
		if (id != null && id !=0 ) {
			
			model.addAttribute("persona", personaService.get(id));
		} else {

			model.addAttribute("persona", new Persona());
		}	
		
		return "save";
	}
	/*---------------*/
	
	@PostMapping("/save")
	public String save(@Valid Persona persona, BindingResult result, Model model, RedirectAttributes attribute) {
		
		if (result.hasErrors()) {
			
			model.addAttribute("listPaises",paisService.getAll());
			
			System.out.println("Existieron errores en el formulario");
			
			return "save";// envia error a la pag "save.html"
		}
		
		
		personaService.save(persona);
		
		attribute.addFlashAttribute("success", "Cliente registrado exitosamente");
		
		return "redirect:/listpersona"; //Me redirecciona al LISTADO
	}
	/*---------------*/
	
	
	
	@GetMapping("/delete/{id}")
	public String deleteX(@PathVariable("id") Integer id, Model model, RedirectAttributes attribute) {
		
		personaService.delete(id);
		
		attribute.addFlashAttribute("warning", "Cliente eliminado");
		
		return "redirect:/listpersona";
	}
	
	

}
