package com.ejemplo2.service;

import java.util.Optional;

import com.ejemplo2.commons.IGenericService;
import com.ejemplo2.model.Usuario;



public interface IUsuarioService extends IGenericService<Usuario, Integer> {
	
	public Optional<Usuario> findByUsername(String username);

}
