package com.ejemplo2.service.implement;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.ejemplo2.commons.GenericServiceImplement;
import com.ejemplo2.dao.IUsuarioDao;
import com.ejemplo2.model.Usuario;
import com.ejemplo2.service.IUsuarioService;



@Service
public class UsuarioServiceImplement extends GenericServiceImplement<Usuario, Integer> implements IUsuarioService{

	
	@Autowired
	private IUsuarioDao usuarioDao;
	
	
	@Override
	public JpaRepository<Usuario, Integer> getDao() {

		return usuarioDao;
	}

	
	@Override
	public Optional<Usuario> findByUsername(String username) {
		
		return usuarioDao.findByUsername(username);
	}

}
