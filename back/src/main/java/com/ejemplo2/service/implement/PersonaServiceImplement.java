package com.ejemplo2.service.implement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.ejemplo2.commons.GenericServiceImplement;
import com.ejemplo2.dao.IPersonaDao;
import com.ejemplo2.model.Persona;
import com.ejemplo2.service.IPersonaService;



/*@Service -> indica que la clase es un bean de la capa de negocio*/

@Service
public class PersonaServiceImplement extends GenericServiceImplement<Persona, Integer> implements IPersonaService{

	
	@Autowired
	private IPersonaDao personaDao;
	
	
	@Override
	public JpaRepository<Persona, Integer> getDao() {

		return personaDao;
	}

}
