package com.ejemplo2.service.implement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.ejemplo2.commons.GenericServiceImplement;
import com.ejemplo2.dao.IPaisDao;
import com.ejemplo2.model.Pais;
import com.ejemplo2.service.IPaisService;


@Service
public class PaisServiceImplement extends GenericServiceImplement<Pais, Integer> implements IPaisService{

	
	@Autowired
	private IPaisDao paisDao;
	
	
	@Override
	public JpaRepository<Pais, Integer> getDao() {

		return paisDao;
	}

}
