package com.ejemplo2.service;

import com.ejemplo2.commons.IGenericService;
import com.ejemplo2.model.Persona;

public interface IPersonaService extends IGenericService<Persona, Integer> {

}
