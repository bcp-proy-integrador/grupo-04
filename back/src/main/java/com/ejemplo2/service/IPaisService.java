package com.ejemplo2.service;

import com.ejemplo2.commons.IGenericService;
import com.ejemplo2.model.Pais;


public interface IPaisService extends IGenericService<Pais, Integer> {

}
