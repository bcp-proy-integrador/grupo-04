package com.ejemplo2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ejemplo2SpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ejemplo2SpringApplication.class, args);
	}

}
