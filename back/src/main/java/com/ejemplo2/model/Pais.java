package com.ejemplo2.model;


import java.io.Serializable;

import javax.persistence.*;

@Entity
public class Pais implements Serializable{

	@Id
	@GeneratedValue
	@Column(name = "pais_id")
	private int id;

	@Column(name = "pais_nombre")
	private String nombre;


	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
