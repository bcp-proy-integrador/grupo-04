DELETE from pais; 
INSERT INTO pais (pais_id, pais_nombre) VALUES (1,'Peru');
INSERT INTO pais (pais_id, pais_nombre) VALUES (2,'Argentina');
INSERT INTO pais (pais_id, pais_nombre) VALUES (3,'Brasil');

delete from authorities_users;
delete from authority;
delete from usuario;

INSERT INTO usuario (id,enabled,password,username) VALUES(1,true,'$2a$04$Rdk73vMslUwl6SJhcSd8aOLns7V8DLMQGa0SrgTwOzbuOsBRTtTZK','admin'); -- password: 1234
INSERT INTO usuario (id,enabled,password,username) VALUES(2,true,'$2a$04$eFyQ5XDrVBrLelaVVEpH0uBVESfVZEiArJfdZFfMWL0AQry7Eifbe','user'); -- password: 1234

INSERT INTO authority (id,authority) VALUES (1,'ROLE_ADMIN');
INSERT INTO authority (id,authority) VALUES (2,'ROLE_USER');

INSERT INTO authorities_users (usuario_id, authority_id) VALUES (1,1);
INSERT INTO authorities_users (usuario_id, authority_id) VALUES (1,2);
INSERT INTO authorities_users (usuario_id, authority_id) VALUES (2,2);