package com.ejemplo2;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


/* Paso4 - GENERAR EL "Password", PARA ESO ESTOY QUE HAGO UNA "PRUEBA" */

public class PasswordGenerate {
	
	
	public static void main(String[] args) {
		
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(4);
		
        //El String que mandamos al metodo encode es el password que queremos encriptar.
		System.out.println(bCryptPasswordEncoder.encode("1234"));
	}

}
